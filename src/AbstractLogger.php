<?php
namespace Charm\FallbackLogger;

use Psr\Log\{
    LoggerInterface,
    LoggerTrait
};

abstract class AbstractLogger implements LoggerInterface {
    use LoggerTrait;

    /**
     * Interpolates context values into the message placeholders.
     *
     * Copied from {@see https://www.php-fig.org/psr/psr-3/}
     */
    protected static function interpolate($message, array $context) {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be cast to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
