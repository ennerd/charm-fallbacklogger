<?php
namespace Charm\FallbackLogger;

use Stringable;
use Charm\Terminal;
use Psr\Log\LogLevel;

class StreamLogger extends AbstractLogger {

    private $terminal;
    private $prefix;

    public function __construct($resource, $prefix=null) {
        if (!is_resource($resource)) {
            throw new \TypeError("Expecting a stream resource in argument 1");
        }
        if ($prefix && !is_string($prefix)) {
            throw new \TypeError("Expecit a string or NULL in argument 2");
        }
        $this->terminal = new Terminal($resource);
        $this->prefix = $prefix ?? null;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     */
    public function log($level, Stringable|string $message, array $context = []): void {
        $message = self::interpolate($message, $context);
        $parts = [];
        $parts[] = "<!silver>".gmdate('Y-m-d H:i:s')."<!>";
        $tab = str_repeat(" ", 10 - strlen($level));
        switch ($level) {
            case LogLevel::EMERGENCY:
            case LogLevel::ALERT:
            case LogLevel::CRITICAL:
                $parts[] = "[<!fuchsiaBackground><!white>$level<!!>]".$tab;
                $message = "<!white>$message<!>";
                break;
            case LogLevel::ERROR:
                $parts[] = "[<!red>$level<!>]".$tab;
                break;
            case LogLevel::WARNING:
                $parts[] = "[<!darkOrange>$level<!>]".$tab;
                break;
            case LogLevel::NOTICE:
                $parts[] = "[<!white>$level<!>]".$tab;
                break;
            case LogLevel::INFO:
                $parts[] = "[<!lime>$level<!>]".$tab;
                break;
            case LogLevel::DEBUG:
                $parts[] = "[<!yellow>$level<!>]".$tab;
                break;
            default:
                $parts[] = "[<!silver>$level<!>]".$tab;
                break;
        }

        if ($this->prefix !== null) {
            $parts[] = "<!italic>".$this->prefix."<!>";
        }

        $parts[] = $message;

        $output = implode(" ", $parts);

        if (strpos($output, "\n") !== false) {
            $output = str_replace("\n", "\n".str_repeat(" ", 33), $output);
        }

        $this->terminal->write($output."\n");
    }

    /**
     * Interpolates context values into the message placeholders.
     *
     * Copied from {@see https://www.php-fig.org/psr/psr-3/}
     */
    protected static function interpolate($message, array $context) {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be cast to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = "<!underline>".$val."<!>";
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
