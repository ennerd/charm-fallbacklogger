<?php
namespace Charm\FallbackLogger;

use Stringable;
use Psr\Log\{
    LoggerInterface,
    LoggerTrait,
    LogLevel
};

class ErrorLogLogger extends AbstractLogger {
    use LoggerTrait;

    private $prefix;

    public function __construct($prefix = null) {
        if ($prefix && !is_string($prefix)) {
            throw new \TypeError("Expecit a string or NULL in argument 2");
        }
        $this->prefix = $prefix;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     */
    public function log($level, Stringable|string $message, array $context = []): void {
        $message = self::interpolate($message, $context);
        $parts = [];
        $tab = str_repeat(" ", 10 - strlen($level));
        switch ($level) {
            case LogLevel::EMERGENCY:
            case LogLevel::ALERT:
            case LogLevel::CRITICAL:
                $parts[] = "[$level]".$tab;
                break;
            case LogLevel::ERROR:
                $parts[] = "[$level]".$tab;
                break;
            case LogLevel::WARNING:
                $parts[] = "[$level]".$tab;
                break;
            case LogLevel::NOTICE:
                $parts[] = "[$level]".$tab;
                break;
            case LogLevel::INFO:
                $parts[] = "[$level]".$tab;
                break;
            case LogLevel::DEBUG:
                $parts[] = "[$level]".$tab;
                break;
            default:
                $parts[] = "[$level]".$tab;
                break;
        }

        if ($this->prefix !== null) {
            $parts[] = $this->prefix;
        }

        $parts[] = $message;

        \error_log(implode(" ", $parts));
    }
}
