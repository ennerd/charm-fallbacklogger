<?php
namespace Charm;

use Psr\Log\LoggerInterface;

/**
 * Provides a fallback PSR-3 logger instance for applications that require one.
 */
class FallbackLogger {

    private static ?LoggerInterface $instance = null;

    public static function get(): LoggerInterface {
        if (self::$instance === null) {
            if (\PHP_SAPI === 'cli') {
                self::$instance = new FallbackLogger\StreamLogger(STDERR);
            } else {
                self::$instance = new FallbackLogger\ErrorLogLogger();
            }
        }
        return self::$instance;
    }

    public static function set(LoggerInterface $logger): void {
        self::$instance = $logger;
    }
}
