<?php
require(__DIR__.'/../vendor/autoload.php');

use Charm\FallbackLogger\ErrorLogLogger;

set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext) {
    var_dump(func_get_args());
});

$logger = new ErrorLogLogger();

$logger->emergency("This is an {logLevel}", ['logLevel' => 'emergency']);
$logger->alert("This is an {logLevel}", ['logLevel' => 'alert']);
$logger->critical("This is an {logLevel}", ['logLevel' => 'critical']);
$logger->error("This is an {logLevel}", ['logLevel' => 'error']);
$logger->warning("This is an {logLevel}", ['logLevel' => 'warning']);
$logger->notice("This is an {logLevel}", ['logLevel' => 'notice']);
$logger->info("This is an {logLevel}", ['logLevel' => 'info']);
$logger->debug("This is an {logLevel}", ['logLevel' => 'debug']);
